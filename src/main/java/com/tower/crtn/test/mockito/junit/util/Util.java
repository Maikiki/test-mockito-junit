package com.tower.crtn.test.mockito.junit.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tower.crtn.test.mockito.junit.entity.CustomerEntity;
import com.tower.crtn.test.mockito.junit.model.CustomerModel;
import com.tower.crtn.test.mockito.junit.model.CustomerUpdateModel;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Util {

  public static String getJson(Object obj) {
    String json = "";
    try {
      ObjectMapper mapper = new ObjectMapper();
      json = mapper.writeValueAsString(obj);
    } catch (Exception ex) {
      log.error("Error in Util.getJson: " + ex.getMessage());
    }
    return json;
  }

  public static CustomerEntity converterCustomerModelToCustomerEntity(CustomerModel model) {
    CustomerEntity entity = new CustomerEntity();
    entity.setCustomerId(model.getCustomerId());
    entity.setName(model.getName());
    entity.setLastName(model.getLastName());
    entity.setBirthDate(model.getBirthDate());
    entity.setStatusCode(model.getStatusCode());
    return entity;
  }

  public static CustomerEntity converterCustomerUpdateModelToCustomerEntity(int customerId,
      CustomerUpdateModel model) {
    CustomerEntity entity = new CustomerEntity();
    entity.setCustomerId(customerId);
    entity.setName(model.getName());
    entity.setLastName(model.getLastName());
    entity.setBirthDate(model.getBirthDate());
    entity.setStatusCode(model.getStatusCode());
    return entity;
  }

  public static CustomerModel converterCustomerEntityToCustomerModel(CustomerEntity entity) {
    CustomerModel model = new CustomerModel();
    model.setCustomerId(entity.getCustomerId());
    model.setName(entity.getName());
    model.setLastName(entity.getLastName());
    model.setBirthDate(entity.getBirthDate());
    model.setStatusCode(entity.getStatusCode());
    return model;
  }
}
