package com.tower.crtn.test.mockito.junit.controller;

import com.tower.crtn.test.mockito.junit.model.CustomerModel;
import com.tower.crtn.test.mockito.junit.model.CustomerUpdateModel;
import com.tower.crtn.test.mockito.junit.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("${properties.api.uri.base-path}")
public class TestController {

  @Autowired
  private CustomerService customerService;

  @PutMapping(value = "${properties.api.uri.specific-paths.save-customer}")
  public ResponseEntity<Void> saveCustomer(CustomerModel customerModel) {
    customerService.saveCustomer(customerModel);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  @PostMapping(value = "${properties.api.uri.specific-paths.update-customer}")
  public ResponseEntity<Void> updateCustomer(int customerId, CustomerUpdateModel customerModel) {
    customerService.updateCustomer(customerId, customerModel);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  @DeleteMapping(value = "${properties.api.uri.specific-paths.delete-customer}")
  public ResponseEntity<Void> deleteCustomer(int customerId) {
    customerService.deleteCustomer(customerId);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  @GetMapping(value = "${properties.api.uri.specific-paths.find-all}")
  public ResponseEntity<List<CustomerModel>> getAllCustomers() {
    List<CustomerModel> customers = customerService.findAllCustomers();
    return new ResponseEntity<>(customers, HttpStatus.OK);
  }

  @PostMapping(value = "${properties.api.uri.specific-paths.find-by-customer-id}")
  public ResponseEntity<CustomerModel> getByCustomerId(int customerId) {
    CustomerModel customerModel = customerService.findByCustomerId(customerId);
    return new ResponseEntity<>(customerModel, HttpStatus.OK);
  }

  @PostMapping(value = "${properties.api.uri.specific-paths.find-by-full-name}")
  public ResponseEntity<CustomerModel> getByFullName(String name, String lastName) {
    CustomerModel customerModel = customerService.findByFullName(name, lastName);
    return new ResponseEntity<>(customerModel, HttpStatus.OK);
  }

  @PostMapping(value = "${properties.api.uri.specific-paths.find-all-by-status-code}")
  public ResponseEntity<List<CustomerModel>> getAllByStatusCode(String statusCode) {
    List<CustomerModel> customers = customerService.findAllByStatusCode(statusCode);
    return new ResponseEntity<>(customers, HttpStatus.OK);
  }
}
